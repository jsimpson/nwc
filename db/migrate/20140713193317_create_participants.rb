class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.string :name
      t.integer :department_id

      t.timestamps
    end
    add_index :participants, [:department_id, :created_at]
  end
end
