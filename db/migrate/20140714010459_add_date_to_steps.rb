class AddDateToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :date, :datetime
  end
end
