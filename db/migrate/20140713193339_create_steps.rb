class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :num_steps
      t.integer :participant_id

      t.timestamps
    end
    add_index :steps, [:participant_id, :created_at]
  end
end
