require 'spec_helper'

describe "Participant pages" do
  subject { page }

  describe "index" do
    let(:user) { FactoryGirl.create(:user) }
    let(:participant) { FactoryGirl.create(:participant) }

    before do
      sign_in user
      FactoryGirl.create(:participant)
      visit participants_path
    end

    it { should have_title('Participants') }
    it { should have_selector('h1', text: "Participants") }

    it "should list each participant" do
      Participant.all.each { |participant| expect(page).to have_selector('td', text: participant.name) }
    end

    describe "pagination" do
      before(:all) { 30.times { FactoryGirl.create(:participant) } }
      after(:all) { Participant.delete_all }

      it { should have_selector('div.pagination') }

      it "should list each participant" do
        Participant.paginate(page: 1).each do |participant|
          expect(page).to have_selector('td', text: participant.name)
        end
      end
    end

    describe "delete links" do
      it { should_not have_link('delete') }

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit participants_path
        end

        it { should have_link('Delete', href: participant_path(Participant.last)) }

        it "should be able to delete a participant" do
          expect { (click_link('Delete', match: :first)).to change(Participant, :count).by(-1) }
        end
      end
    end

    describe "profile pages" do
      let!(:s1) { FactoryGirl.create(:step, num_steps: 12345, participant_id: participant.id) }
      let!(:s2) { FactoryGirl.create(:step, num_steps: 9999, participant_id: participant.id) }
      before { visit participant_path(participant) }

      it { should have_selector('h1', participant.name) }
      it { should have_title(participant.name) }

      describe "steps" do
        it { should have_selector('td', text: "12,345") }
        it { should have_selector('td', text: "9,999") }
        it { should have_content(participant.steps.count) }
      end
    end
  end
end
