require 'spec_helper'

describe "Department pages" do
  subject { page }

  describe "index" do
    let(:user) { FactoryGirl.create(:user) }
    let(:department) { FactoryGirl.create(:department) }

    before do
      sign_in user
      visit departments_path
    end

    it { should have_title('Departments') }
    it { should have_selector('h1', text: "Departments") }

    it "should list the department" do
      Department.first { |department| expect(page).to have_selector('td', text: department.name) }
    end

    describe "pagination" do
      before do
        sign_in user
        31.times { FactoryGirl.create(:department) }
        visit departments_path
      end
      after(:all) { Department.delete_all }

      it { should have_selector('div.pagination') }

      it "should list each department" do
        Department.paginate(page: 1).each do |dept|
          expect(page).to have_selector('td', text: dept.name)
        end
      end
    end

    describe "delete links" do
      before(:all) { FactoryGirl.create(:department) }
      after(:all) { Department.delete_all }

      it { should_not have_link('delete') }

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit departments_path
        end

        it { should have_link('Delete', href: department_path(Department.first)) }

        it "should be able to delete a department" do
          expect { (click_link('Delete', match: :first)).to change(Department, :count).by(-1) }
        end
      end
    end

    describe "profile pages" do
      let!(:p1) { FactoryGirl.create(:participant, department_id: department.id) }
      let!(:p2) { FactoryGirl.create(:participant, department_id: department.id) }
      before { visit department_path(department) }

      it { should have_selector('h1', department.name) }
      it { should have_title(department.name) }

      describe "participants" do
        it { should have_selector('td', text: p1.name) }
        it { should have_selector('td', text: p2.name) }
      end
    end
  end
end
