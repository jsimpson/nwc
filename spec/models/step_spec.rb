require 'spec_helper'

describe Step do
  let(:participant) { FactoryGirl.create(:participant) }
  before { @step = participant.steps.build(num_steps: 12345) }

  subject { @step }

  it { should respond_to(:num_steps) }
  it { should respond_to(:participant_id) }
  it { should respond_to(:participant) }
  it { should be_valid }

  describe "when participant_id is not present" do
    before { @step.participant_id = nil }
    it { should_not be_valid }
  end

  describe "without number of steps" do
    before { @step.num_steps = nil }
    it { should_not be_valid }
  end

  describe "when number of steps is too big" do
    before { @step.num_steps = 100000 }
    it { should_not be_valid }
  end
end
