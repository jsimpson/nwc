require 'spec_helper'

describe Participant do
  let(:participant) { FactoryGirl.create(:participant) }

  it { should respond_to(:name) }
  it { should respond_to(:department) }
  it { should respond_to(:steps) }

  describe "step associations" do
    before { participant.save }
    let!(:older_step) do
      FactoryGirl.create(:step, participant: participant, date: 2.day.ago)
    end
    let!(:newer_step) do
      FactoryGirl.create(:step, participant: participant, date: 1.day.ago)
    end

    it "should have the right steps in the right order" do
      expect(participant.steps.to_a).to eq [newer_step, older_step]
    end

    it "should destroy associated steps" do
      steps = participant.steps.to_a
      participant.destroy
      expect(steps).not_to be_empty
      steps.each do |step|
        expect(Step.where(id: step.id)).to be_empty
      end
    end
  end
end
