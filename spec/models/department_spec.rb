require 'spec_helper'

describe Department do
  let(:department) { FactoryGirl.create(:department) }
  subject { department }

  it { should respond_to(:name) }
  it { should respond_to(:participants) }

  describe "when name is not present" do
    before { department.name = "" }
    it { should_not be_valid }
  end

  describe "when name is too long" do
    before { department.name = "a" * 101 }
    it { should_not be_valid }
  end
end
