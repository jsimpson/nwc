# Neovia Walking Challenge

A simple app for loading and tracking participants and their steps in (my companies) Neovia Walking Challenge. The Neovia website does not provide many options for viewing data and this app aims to do little more than play around with RoR and of course give me more insight into the competition.

I also have written a web scraper to grab the data in to a hash with a companion rake task to update the data, but it is currently unreleased. Scraping the site requires a web driver that can execute JavaScript.
