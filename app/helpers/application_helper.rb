module ApplicationHelper
  def full_title(page_title)
    base_title = "InTouch Neovia Walking Challenge"
    base_title if page_title.empty?
    "#{base_title} | #{page_title}"
  end
end
