class Step < ActiveRecord::Base
  belongs_to :participant
  default_scope -> { order('date DESC') }
  validates :participant_id, presence: true
  validates :num_steps, presence: true, numericality: { only_integer: true, less_than: 100000 }

  def self.total
    sum(:num_steps)
  end

  def self.avg
    average(:num_steps).to_i
  end
end
