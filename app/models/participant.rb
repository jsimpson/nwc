class Participant < ActiveRecord::Base
  belongs_to :department
  validates :department_id, presence: true
  has_many :steps, dependent: :destroy
end
