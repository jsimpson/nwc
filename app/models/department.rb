class Department < ActiveRecord::Base
  has_many :participants
  default_scope -> { order('name ASC') }
  validates :name, presence: true, uniqueness: true, length: { maximum: 100 }

  def self.total
    sum(:participants)
  end
end
