class StaticPagesController < ApplicationController
  def home
    if signed_in?
      @participants = Participant.all.sort { |a,b| b.steps.total <=> a.steps.total }.take(10)
    end
  end
end
