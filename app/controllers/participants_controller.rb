class ParticipantsController < ApplicationController
  before_action :signed_in_user

  def index
    @participants = Participant.paginate(page: params[:page])
  end

  def show
    @participant = Participant.find(params[:id])
    @steps = @participant.steps.paginate(page: params[:page])
    steps_json = @participant.steps

    respond_to do |format|
      format.html
      format.json { render :json => steps_json }
    end
  end

  def new
    @participant = Participant.new
  end

  def create
    @department = Department.find(params[:department_id])
    @participant = Participant.new(participant_params)
    if @participant.save
      redirect_to participants_url
    else
      render 'new'
    end
  end

  def edit
    @participant = Participant.find(params[:id])
  end

  def destroy
    Participant.find(params[:id]).destroy
    flash.now[:success] = "Participant successfully deleted"
    redirect_to participants_url
  end

  def update
    if @participant.update_attributes(participant_params)
      flash.now[:success] = "Changes saved"
      redirect_to @participant
    else
      render 'edit'
    end
  end

  private

  def participant_params
    params.require(:participant).permit(:name, :department_id)
  end
end
