class DepartmentsController < ApplicationController
  before_action :signed_in_user

  def index
    @departments = Department.paginate(page: params[:page])
  end

  def show
    @department = Department.find(params[:id])
    @participants = @department.participants.paginate(page: params[:page])
    @total_steps = @participants.map { |p| p.steps.total }.sum
  end

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department)
    if @department.save
      redirect_to departments_url
    else
      render 'new'
    end
  end

  def edit
    @department = Department.find(params[:id])
  end

  def destroy
    Department.find(params[:id]).destroy
    flash.now[:success] = "Department successfully deleted"
    redirect_to departments_url
  end

  def update
    @department = Department.find_by_id(params[:id])
    if @department.update_attributes(department_params)
      flash.now[:success] = "Changes saved"
      redirect_to @department
    else
      render 'edit'
    end
  end

  private

  def department_params
    params.require(:department).permit(:name)
  end
end
