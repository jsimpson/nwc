class StepsController < ApplicationController
  before_action :signed_in_user

  def index
  end

  def new
    @step = Step.new
  end

  def create
    @participant = Participant.find(params[:participant_id])
    @step = @participant.steps.build(steps_params)
    if @step.save
      flash.now[:success] = "Step created"
    end
    redirect_to participant_url(@participant)
  end

  def destroy
    @participant = Participant.find(params[:participant_id])
    @participant.step.destroy
    redirect_to root_url
  end

  private

  def steps_params
    params.require(:step).permit(:participant_id, :date, :num_steps)
  end
end
