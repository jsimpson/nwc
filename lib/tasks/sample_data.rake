namespace :db do
  DEPARTMENTS = [
    "Accounting/Finance",
    "Clinical Services",
    "Executive/Senior Management",
    "Human Resources",
    "IT/Technical Services",
    "Manufacturing/Quality Assurance",
    "Marketing",
    "Product Specialists",
    "Research & Development",
    "Sales/Operations"
  ]

  STEP_DAYS = [
    1.day.ago,
    2.day.ago,
    3.day.ago,
    4.day.ago,
    5.day.ago
  ]

  task populate: :environment do
    100.times do |n|
      name = Faker::Name.name
      email = "user-#{n+1}@example.com"
      password = "password"
      admin = true
      User.create!(
        name: name,
        email: email,
        password: password,
        password_confirmation: password,
        admin: admin
      )
    end

    10.times do |n|
      name = DEPARTMENTS[n]
      get_department_ids << Department.create!(name: name)
    end

    100.times do |n|
      name = Faker::Name.name
      department_id = get_department_ids.sample
      participant = Participant.create!(
        name: name,
        department_id: department_id
      )
      5.times do |day|
        Step.create!(
          participant_id: participant.id,
          num_steps: 12345,
          date: STEP_DAYS[day]
        )
      end
    end
  end

  def get_department_ids
    @department_ids ||= []
  end
end
